# README #

Swifty VIPER-controller template with Swinject DI Container.

### How do I get set up? ###

* Install Generamba via gem install generamba
* Add line to Rambafile: - {name: viper_swift, git: 'https://gitlab.com/N7Dev/viper_swift.git'}
* Install template with generamba template install
